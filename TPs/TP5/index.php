<style>
table{
	display: table;
	border-collapse: collapse;
	
}
tr{
	display: table-row;

}
td,th{
	width: 40px;
	
	border: 1px solid blue;
	font-size: 18px;
	text-align: center;	

}
th{
	background-color: gray;
}
</style>
<h1>TP5</h1>
<h2>Exercice1</h2>
<h3>Q1</h3>
<?php
$Etudiants=array(
			array("nom"=>"Nali","prenom"=>"Ali","note"=>15),
			array("nom"=>"Nahmed","prenom"=>"Ahmed","note"=>18),
			array("nom"=>"Namina","prenom"=>"Amina","note"=>13)
			);
print_r($Etudiants);
echo "<h3>Q2</h3>";

function afficheTable($E){
	echo "<table><tr>";

	foreach($E[0] as $k=>$v)
		echo "<th>".$k."</th>";
	echo "</tr>";

	foreach($E as $e) {
		echo "<tr>";
		foreach($e as $k=>$v)
				echo "<td>".$v."</td>";
		echo "</tr>";
	}
	echo "</tr></table>";
}

afficheTable($Etudiants);
echo "<h3>Q3:</h3>";

function getMax($E){
	$max=0;
	$mIdx=0;
	foreach ($E as $k=>$v) {
		if($v["note"]>$max){
			$max=$v["note"];
			$mIdx=$k;
		}
	}
	return $mIdx;
}

$idx=getMax($Etudiants);
echo "<h3> Note Max: ".$Etudiants[$idx]["nom"]." => ".$Etudiants[$idx]["note"];
echo "<h3>Q4:</h3>";
$e=array("nom"=>"Nyassine","prenom"=>"yassine","note"=>16);
array_push($Etudiants, $e);
afficheTable($Etudiants);

echo "<h3>Q5:</h3>";
asort($Etudiants);
afficheTable($Etudiants);

$E=$Etudiants;
$result=array();
echo "<br>";
for($j=0;$j<count($Etudiants);$j++){
	$i=getMax($E);
	array_push($result, $E[$i]);
	unset($E[$i]);
}
afficheTable($result);

?>