<?php
echo "<pre>";
// affichage d'un tableau
function affiche_tableau ($t) {
  echo "tableau :";
  for ($i=0 ; $i<sizeof($t) ; $i++) {
    echo ' ';
    echo $t[$i];
  }
  echo "\n";
}

// calcul de la moyenne des éléments d'un tableau t
function moyenne ($t) {
  $somme = 0;
  for ($i=0 ; $i<sizeof($t) ; $i++) {
    $somme = $somme + $t[$i];
  }
  echo 'la moyenne vaut ',($somme/sizeof($t)),"\n";
}

// combien ont eu la moyenne dans un tableau de notes t
function onteulamoyenne ($t) {
  $fois = 0;
  for ($i=0 ; $i<sizeof($t) ; $i++) {
    if ($t[$i]>=10) {
      $fois = $fois + 1;
    }
  }
  echo "$fois ont eu la moyenne\n";
}

// meilleure note
function notemax ($t) {
  $max = 0;
  for ($i=0 ; $i<sizeof($t) ; $i++) {
    if ($t[$i] > $max) {
      $max = $t[$i];
    }
  }
  echo "la note maximale est $max\n";
}

// teste si quelqu'un a eu 20
function ya20 ($t) {
  $i = 0;
  while (($i<sizeof($t)) && ($t[$i]!=20)) {
    $i = $i+1;
  }
  if ($i<sizeof($t)) {
    echo "quelqu'un a eu 20 !\n";
  } else {
    echo "personne n'a eu 20 !\n";
  }
}


// appels aux procédures définies ci-dessus sur un tableau de notes

// définition d'un tableau
$notes    = array(5,12,8,20,10);
// ajout d'un élément
$notes[]  = 13.5;
// modification de la case numéro 2
$notes[2] = 9;
// affichage du tableau par php
print_r($notes);
// affichage de la taille du tableau
echo sizeof($notes);
echo " cases dans ce tableau\n";
// affichage par notre procédure
affiche_tableau($notes);
// moyenne de la promo
moyenne($notes);
// combien de personnes ont eu la moyenne ?
onteulamoyenne($notes);
// quelle est la note maximale obtenue ?
notemax($notes);
// est-ce quelqu'un a eu 20 ?
ya20($notes);
echo "</pre>";
?>